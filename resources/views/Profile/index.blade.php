@extends('Layout.master')
@section('content')
    <div class="row col-md-5">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Address</th>
                <th>Contact No</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($profiles as $profile)
            <tr>
                <td>{{$profile->name}}</td>
                <td>{{$profile->email}}</td>
                <td>{{$profile->address}}</td>
                <td>{{$profile->contact}}</td>
                <td>
                    <a href="{{url('profile/show',[$profile->id])}}" class="btn btn-primary">View</a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection