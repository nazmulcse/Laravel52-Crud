<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Profile;

class ProfileController extends Controller
{
    public function getIndex(){
        $profiles=Profile::all();
        return  view('Profile.index',compact('profiles'));
    }
    public function getCreate(){
        return view('Profile.create');
    }
    public function postStore(Requests\ProfileFormRequest $request){
        $input=$request->all();
        Profile::create($input);
       return redirect('/profile/create');
        //return redirect::action('ProfileController@index');
    }
    public function getShow($id){
        $profile=Profile::find($id);
        //dd($profile);
        return view('Profile.view',compact('profile'));
    }
}
